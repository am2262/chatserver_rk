import java.io.*;
import java.net.*;
import java.util.*;
import java.security.SecureRandom;

class Client { // Class Client
	private Socket socket;
	private String username;
	public Client(Socket socket, String username) { // Create object
		this.socket = socket;
		this.username = username;
	}
	public Socket getSocket() { // Get socket of the client
		return this.socket;
	}
	public String getUsername() { // Get username of the client
		return this.username;
	}
	public void setUsername(String newUsername) { // Set username to the client
		this.username = newUsername;
	}
}

public class ChatServer {
	protected int serverPort = 1234;

	public static ArrayList<Client> clientes = new ArrayList<>(); // Vsi clienti in njihovi podatki

	public static void main(String[] args) throws Exception {
		new ChatServer();
	}

	public static ArrayList<Client> getList() { // Method used to get arraylist
		return clientes;
	}

	public ChatServer() {
		ServerSocket serverSocket = null;

		// create socket
		try {
			serverSocket = new ServerSocket(this.serverPort); // create the ServerSocket
		} catch (Exception e) {
			System.err.println("[system] Could not create socket on port " + this.serverPort);
			e.printStackTrace(System.err);
			System.exit(1);
		}

		// start listening for new connections
		System.out.println("[system] Listening ...");
		try {
			while (true) {
				Socket newClientSocket = serverSocket.accept(); // wait for a new client connection
				synchronized(this) {
					clientes.add(new Client(newClientSocket, null)); // Add user to Arraylist
				}
				ChatServerConnector conn = new ChatServerConnector(this, newClientSocket); // create a new thread for communication with the new client
				conn.start(); // run the new thread
			}
		} catch (Exception e) {
			System.err.println("[error] Accept failed.");
			e.printStackTrace(System.err);
			System.exit(1);
		}

		// close socket
		System.out.println("[system] Closing server socket ...");
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}

	// send a message to all clients connected to the server
	public void sendToClients(String message, String reciever, Socket sender) throws Exception {
		if (reciever == null) { // This means that this is broadcast message
			for (int k = 0; k < clientes.size(); k++) { // Send to everyone
				Socket socket = clientes.get(k).getSocket();
				sendTo(message, socket);
			}
		} else {
			Socket socket = null;
			for (int k = 0; k < clientes.size(); k++) { // Iterate over Arraylist
				if (clientes.get(k).getUsername().equals(reciever)) { // Find user by username
					socket = clientes.get(k).getSocket(); // Find socket
					break;
				}
			}
			if (socket == null) {
				sendTo("[ERROR] Reciever does not exist!", sender);
			} else {
				sendTo(message, sender); // Send message to another method
			}
		}
	}

	public void sendTo(String message, Socket socket) { // Method used to send message to specific socket
		try {
			DataOutputStream out = new DataOutputStream(socket.getOutputStream()); // create output stream for sending messages to the client
			out.writeUTF(message); // send message to the client
		} catch (Exception e) {
			System.err.println("[system] Could not send message to a client");
			e.printStackTrace(System.err);
		}
	}

	public void removeClient(Socket socket) {
		synchronized(this) {
			for (int i = 0; i < clientes.size(); i++) {
				if (socket == clientes.get(i).getSocket()) {
					clientes.remove(i);
					break;
				}
			}
		}
	}
}

class ChatServerConnector extends Thread {
	private ChatServer server;
	private Socket socket;
	final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; // Seed
	static SecureRandom rnd = new SecureRandom();

	public ChatServerConnector(ChatServer server, Socket socket) {
		this.server = server;
		this.socket = socket;
	}

	String randomString(int len){ // Method creates random username
		StringBuilder sb = new StringBuilder(len);
		for( int i = 0; i < len; i++) {
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}

	public void run() {
		System.out.println("[system] Connected with " + this.socket.getInetAddress().getHostName() + ":" + this.socket.getPort());
		DataInputStream in;
		try {
			in = new DataInputStream(this.socket.getInputStream()); // create input stream for listening for incoming messages
		} catch (IOException e) {
			System.err.println("[system] Could not open input stream!");
			e.printStackTrace(System.err);
			this.server.removeClient(socket);
			return;
		}

		while (true) { // infinite loop in which this thread waits for incoming messages and processes them
			String msg_received;
			//String sender;
			String sender = null;
			String reciever = null;
			String msg_send = null;
			boolean com = false; // Used when command is sent

			try {
				msg_received = in.readUTF(); // read the message from the client
				for (int i = 0; i < ChatServer.getList().size(); i++) {
					if (this.socket == ChatServer.getList().get(i).getSocket()) {
						sender = ChatServer.getList().get(i).getUsername(); // Find username of sender
					}
				}
			} catch (Exception e) {
				System.err.println("[system] There was a problem while reading message client on port " + this.socket.getPort());
				e.printStackTrace(System.err);
				this.server.removeClient(this.socket);
				return;
			}

			if (msg_received.length() == 0) {// invalid message
				continue;
			}

			String[] msg = msg_received.split("<¸¸>"); // Split to message and timestamp
			// msg[0] is message
			// msg[1] is timestamp

			if (msg[0].startsWith("/")) { // This a command to server
				String command = msg[0].split(" ")[0].substring(1); // Extract command string from message
				switch (command) { // Might add additional functions to chatroom
					case "rename": // In case you want to rename an user
						String username = msg[0].split(" ")[1]; // Extract username you want to be renamed to
						for (int i = 0; i < ChatServer.getList().size(); i++) { // Iterate over client arraylist
							if (username.equals(ChatServer.getList().get(i).getUsername())) { // Find if username already exists
								try {
									this.server.sendToClients("This username already exists, please choose a new one", username, this.socket); // Warning for existing username
									this.server.sendToClients("Recieving random username", username, this.socket); // Warn user who recieves random username
									username = randomString(6); // Create 6 chars long username
									for (int f = 0; f < ChatServer.getList().size(); f++) { // Iterate over client arraylist
										if (this.socket == ChatServer.getList().get(f).getSocket()) { // Find senders socket id
											ChatServer.getList().get(f).setUsername(username); // Append random username to user
										}
									}
								} catch (Exception e) {
									System.err.println("[system] There was a problem while sending the message to all clients");
									e.printStackTrace(System.err);
								}
								break; // Finish before you allow adding existing username
							}
						}
						// If username is unique
						for (int i = 0; i < ChatServer.getList().size(); i++) { // Iterate over client arraylist
							if (this.socket == ChatServer.getList().get(i).getSocket()) { // Find user's socket
								ChatServer.getList().get(i).setUsername(username); // Append desired username
								System.out.println("User " + sender + " at: " + this.socket.getPort() + " renamed himself to: " + username); // print the incoming message in the console
								try {
									this.server.sendToClients("You renamed yourself to: " + username, username, this.socket); // send message to a specific client
								} catch (Exception e) {
									System.err.println("[system] There was a problem while sending the message to all clients");
									e.printStackTrace(System.err);
									continue;
								}
								break; // No need to continue
							}
						}
						break;
					default:
						break;
				}
				com = true; // Note that this is not an usual message
			} else if (msg[0].startsWith("@")) { // This is private message
				reciever = msg[0].split(" ")[0].substring(1); // Extract reciever of the message
				msg_send = "[PRIVATE]" + msg[1] + " " + sender + ": " + msg[0]; // Compose message

			} else { // Ordinary message
				msg_send = msg[1] + " " + sender + ": " + msg[0]; // Compose ordinary message with timestamp and name
			}

			if (!com) { // Only if this isn't a command
				System.out.println("[RKchat] [" + sender + "]: " + msg[0]); // print the incoming message in the console

				try {
					this.server.sendToClients(msg_send, reciever, this.socket); // send message to a specific client
				} catch (Exception e) {
					System.err.println("[system] There was a problem while sending the message to all clients");
					e.printStackTrace(System.err);
					continue;
				}
			}
		}
	}
}
