import java.io.*;
import java.net.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ChatClient extends Thread
{
	protected int serverPort = 1234;

	public static void main(String[] args) throws Exception {
		new ChatClient();
	}

	public ChatClient() throws Exception {
		Socket socket = null;
		DataInputStream in = null;
		DataOutputStream out = null;
		String username = null;

		Scanner sc = new Scanner(System.in);

		// connect to the chat server
		try {
			System.out.println(
					"Super Duper Chatroom\n" +
					"How do I use it?\n" +
					"\t- First enter your username when asked\n" +
					"\t- To rename yourself, write '/rename newUsername'\n" +
					"\t- To send private message, write '@user message'");
			System.out.println("[system] Connecting to chat server ...");
			System.out.print("Insert your new username: ");
			socket = new Socket("localhost", serverPort); // create socket connection
			in = new DataInputStream(socket.getInputStream()); // create input stream for listening for incoming messages
			out = new DataOutputStream(socket.getOutputStream()); // create output stream for sending messages
			username = sc.nextLine(); //Read username
			this.sendMessage("/rename " + username, out); // Send username to server
			System.out.println("[system] Connected");

			ChatClientMessageReceiver message_receiver = new ChatClientMessageReceiver(in); // create a separate thread for listening to messages from the chat server
			message_receiver.start(); // run the new thread

		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}

		// read from STDIN and send messages to the chat server
		BufferedReader std_in = new BufferedReader(new InputStreamReader(System.in));

		String userInput;
		String message = null;
		while ((userInput = std_in.readLine()) != null) { // read a line from the console
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"); // Create timestamp
			String time = "[" + dtf.format(LocalDateTime.now()) + "]"; // Create timestamp
			message = userInput + "<¸¸>" + time + "<¸¸>" + username;
			this.sendMessage(message, out); // send the message to the chat server
		}

		// cleanup
		out.close();
		in.close();
		std_in.close();
		socket.close();
	}

	private void sendMessage(String message, DataOutputStream out) {
		try {
			out.writeUTF(message); // send the message to the chat server
			out.flush(); // ensure the message has been sent
		} catch (IOException e) {
			System.err.println("[system] Could not send message");
			e.printStackTrace(System.err);
		}
	}
}

// wait for messages from the chat server and print them out
class ChatClientMessageReceiver extends Thread {
	private DataInputStream in;

	public ChatClientMessageReceiver(DataInputStream in) {
		this.in = in;
	}

	public void run() {
		try {
			String message;
			while ((message = this.in.readUTF()) != null) { // read new message
				System.out.println("[RKchat] " + message); // print the message to the console
				System.out.print("Write: ");
			}
		} catch (Exception e) {
			System.err.println("[system] Could not read message");
			e.printStackTrace(System.err);
		}
	}
}
