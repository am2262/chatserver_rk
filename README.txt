﻿LDN6: Programiranje vtičev

Ugotovili smo, da je komuniciranje preko foruma spletni učilnici zamudno in nepraktično. 
Študenti bi radi med seboj komunicirali zanesljivo in v realnem času. Na spletu obstaja 
veliko rešitev, ki bi bile primerne, a nobena ne ustreza vsem našim zahtevam. Tako nam 
ne ostane drugega, da kot pravi računalničarji sprogramiramo nekaj svojega.

Na voljo imate dve delni implementaciji (Java in Python), ki pa sta izredno preprosti, 
saj pošiljata le enostavna tekstovna sporočila. Izberite si enega od programskih jezikov 
in dopolnite oba programa (pogovorni strežnik in odjemalec) tako, da bosta sposobna 
pošiljati in sprejemati strukturirane podatke, za kar lahko uporabite format XML, JSON ali kaj svojega.

Zahteve

Pogovorni strežnik
  * Strežnik mora biti večniten (že narejeno)
  * Strežnik mora podpirati hkratno komunikacijo z več odjemalci (že narejeno)
  * Vsako javno sporočilo, ki pride do strežnika, se razpošlje vsem odjemalcem (že narejeno)
  * Privatna sporočila se pošljejo le naslovniku
  * V kolikor naslovnik ni prijavljen v pogovorni strežnik, naj se pošiljatelju pošlje sporočilo o napaki
  * Vsako sporočilo, ki ga strežnik pošilja odjemalcu, mora biti opremljeno z imenom originalnega pošiljatelja

Odjemalec
  * Študent se mora v odjemalca prijaviti s svojim imenom
  * Program se poveže na pogovorni strežnik in pošilja nanj vsa sporočila, ki jih študent vtipka preko standardnega vhoda STDIN (že narejeno)
  * Vsako sporočilo, ki ga študent pošlje na strežnik, mora biti opremljeno z njegovim imenom in časom pošiljanja

Oddaja naloge
  * Oddati morate ZIP (ne uporabljajte drugih kompresij!) datoteko, ki vsebuje vašo implementacijo pogovornega strežnika in odjemalca.



LDN11: SSL/TLS

V tej nalogi boste naredili varnostno nadgradnjo pogovornega strežnika in odjemalca iz LDN6.

Odločili smo se, da bomo za višjo stopnjo varnosti poskrbeli s pomočjo protokola TLS in z uporabo samopodpisanih digitalnih certifikatov (seveda pa vse deluje tudi s "pravimi" certifikati):
  * Strežnik se mora odjemalcu predstaviti z digitalnim certifikatom
	* V polju Common Name strežnikovega certifikata naj se nahaja vrednost localhost
  * Tudi odjemalci se morajo predstaviti strežniku z digitalnim certifikatom!
	* Zgenerirajte digitalne certifikate za vsaj 3 odjemalce - uporabite različne vrednosti za Common Name
	* Vrednost Common Name naj se uporablja na strežniku za identifikacijo odjemalca

Uporabnikovo ime naj se z odjemalca ne pošilja več na strežnik kot del sporočila. Strežnik naj odjemalca identificira na podlagi vrednosti v polju Common Name.

Strežnik naj dovoli priklop le po naslednjih algoritmih v TLS protokolu (kombinacij teh treh komponent, ki določajo t.i. cipher suite):
  * RSA (samostojno, ne želimo ga uporabiti v kombinaciji z DHE)
  * AES z 128 bitnim ključem, CBC veriženje blokov
  * SHA256 za računanje HMAC

Takšen kandidat je le eden - glej dokumentacijo:
*JSSE: http://java.sun.com/j2se/1.5.0/docs/guide/security/jsse/JSSERefGuide.html#AppA
*OpenSSL: http://www.openssl.org/docs/apps/ciphers.html#CIPHER-SUITE-NAMES

V Javi ustrezen string iz dokumentacije za cipher suite nastavite na SSLSocket-u in SSLServerSocket-u z metodo setEnabledCipherSuites(String[]) še preden prvič uporabite SSLSocket oz. SSLServerSocket oz. bolj natančno: še preden se izvede faza rokovanja (SSL Handshake), ki se implicitno izvede pred prvim poslanim znakom ali pa eksplicitno z metodo startHandshake(), ki jo je zato priporočljivo klicati pred branjem imena iz digitalnega potrdila.

Za Python3 uporabite metodo na SSL kontekstu ctx.set_ciphers().

Oddaja naloge
Oddati morate ZIP (ne uporabljajte drugih kompresij!) datoteko, ki vsebuje vašo implementacijo pogovornega strežnika in odjemalca. Oddana datoteka naj vsebuje tudi vse zgenerirane ključe. Za pozitivno oceno mora:
  * Delovati pošiljanje sporočil (vsaj javnih)
  * Med odjemalci in strežnikom se morajo podatki prenašati po kriptirani povezavi za identifikacijo uporabnika se mora uporabiti vrednost polja Common Name iz njegovega digitalnega certifikata